For more information please visit http://drupal.org/node/456

CONTENTS OF THIS FILE
---------------------

 * About Drupal
 * About Personal Site theme
 * Installing Drupal Themes
 

ABOUT DRUPAL
------------

Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, see the Drupal website at http://drupal.org/, and join the
Drupal community at http://drupal.org/community.

Legal information about Drupal:
 * Know your rights when using Drupal:
   See LICENSE.txt in the same directory as this document.
 * Learn about the Drupal trademark and logo policy:
   http://drupal.com/trademark


*** About Personal Site theme
---------------------------------------
The theme is intended for personal websites or blogs. There is one main area to the left
and two columns to the right where different content can be placed.
The first kolumenen is a bit wider than the other.


*** Installing Drupal Themes 
---------------------------------------
1. Access your Web server using an FTP client or Web server administration tools.
2. Create a folder for your specific theme under "<YourSiteFolder>/sites/all/themes/" folder within Drupal installation.
   For example: <YourSiteFolder>/sites/all/themes/<MyNewTheme>
3. Copy or upload theme files into the newly created <MyNewTheme> folder.
4. Login to your Drupal Administer.
5. Go to Drupal Administer -> Appearance (www.YourSite.com/?q=admin/appearance)
6. In the bottom of the page you will find the theme you've downloaded to the location of your site.
7. Click "Enable and set default" below the theme.
For more information please visit: http://drupal.org/node/456


---------------------------------------
by Dhavyd Vanderlei - http://www.dhavyd.com